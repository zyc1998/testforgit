# Reference software for IEEE 1857.11 Standard for Neural Network-Based Image Coding

The software is the reference software for IEEE 1857.11 standard for neural network-based image coding, including encoder, decoder and training functionalities.

Reference software is useful in aiding users of a video coding standard to establish and test conformance and interoperability, and to educate users and demonstrate the capabilities of the standard. For these purposes, this software is provided as an aid for the study and implementation of Neural Network-Based Image Coding.

The software has been developed by the IEEE 1857.11 Working Group.

## Table of Contents

- [Environment](#environment)
- [Usage](#usage)
	- [Encoding](#encoding)
	- [Decoding](#decoding)
	- [Training](#training)
- [Possible Issues](#possible issues)
- [License](#license)
- [Contributors](#contributors)
### Environment
-----------
Please read [Requirements](https://gitlab.com/NIC_software/NIC/-/tree/Encoder_IEEE1857software/requirements.txt) for details on environment of this software.

### Usage
-----------
#### Encoding

* Encode a single picture-->
```bash
python3 Encoder/encode.py --input YourTestPic.png --output OutputBin.bin --device cuda --ckptdir PretrainedModelsFolder --target_rate 0.75 --cfg Encoder/objective.json
```

* Encode all images under a folder-->
```bash
python3 Encoder/encode.py -inputPath YourTestPicFolder -outputPath OutputBinsFolder --device cuda --ckpt PretrainedModelsFolder --target_rate 0.75 --cfg Encoder/objective.json
```

* Arguments Help
```bash
--input: Input image file path.
--output: Output bin file name.
--inputPath: Input images folder path.
--outputPath: Output bins folder path.
--device: CPU or GPU device. You can only use "cpu" or "cuda", defalut is "cuda".
--ckptdir: Checkpoint folder containing multiple pretrained models.
--target_rate: Target bpp. Default is a multiple rate list: "[0.75,0.50,0.25,0.12,0.06]"
--cfg: Path to the CfG file. This file is placed in the "Encoder" folder by default. We introduce the composition of this file in "Cfg Help"
```

* Cfg Help
```bash
The CFG file of encoding mainly consists of two parts: "syntax of coding model selection" and "syntax of picture". You can modify relevant parameters as required.

Example:

"coding_model_seclection_syntax": {
        "bin2symbolIdx": 0,
        "arithmeticEngineIdx": 0,
        "synthesisTransformIdx": 0
    }

"0" denotes BEE task, "1" denotes iWave task, "2" denotes NIC task.

"picture_syntax":{

}

```
#### Decoding

* Decode a single bin-->
```bash
python3 Decoder/decode.py --input YourTestBin.bin --output OutputRecon.bin --device cuda --ckptdir PretrainedModelsFolder
```

* Decode all bins under a folder-->
```bash
python3 Decoder/decode.py --binpath YourTestBinsFolder --recpath OutputReconsFolder --device cuda --ckptdir PretrainedModelsFolder
```

* Arguments Help
```bash
--input: Input bin file path.
--output: Output reconstruction image file name.
--binpath: Input bins folder path.
--recpath: Output reconstruction images folder path.
--device: CPU or GPU device. You can only use "cpu" or "cuda", defalut is "cuda".
--ckptdir: Checkpoint folder containing multiple pretrained models.
```

#### Training

Please read [TRAINING.md](urls) for details on our different tasks of training.

### Possible Issues
-----------
AE integration issues (To be completed)

### License
-----------
This project is licensed under the BSD-3 License - see the [LICENSE](https://gitlab.com/NIC_software/NIC/-/tree/Encoder_IEEE1857software/LICENSE) file for details

### Contributors
-----------
Contact information of Contributors 


